module.exports = class RethinkDriver {
  constructor(host = "localhost", port = 28015) {
    this.r = require("rethinkdbdash")({
      servers: [{ host: host, port: port }],
      silent: true
    });
  }

  dbCreate({ dbName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | dbCreate"));
      let dbList = this.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      if (dbList.includes(dbName)) return resolve();
      try {
        await this.r.dbCreate(dbName);
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }

  dbDrop({ dbName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | dbDrop"));
      let dbList = this.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      if (!dbList.includes(dbName)) return resolve();
      try {
        await this.r.dbDrop(dbName);
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }

  dbList() {
    return new Promise(async (resolve, reject) => {
      let dbList = this.r.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      return resolve(dbList);
    });
  }

  tableCreate({ dbName, tableName, options = {} }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject("Error: Invalid Input | dbName | tableCreate");
      if (typeof tableName != "string") return reject("Error: Invalid Input | tableName | tableCreate");
      if (typeof options != "object") return reject("Error: Invalid Input | options | tableCreate");
      let dbList = this.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      if (!dbList.includes(dbName)) return reject(new Error(`Database ${dbName} does not exist.`));
      let tableList = this.tableList({ dbName });
      try {
        tableList = await tableList;
      } catch (err) {
        return reject(err);
      }
      if (tableList.includes(tableName)) return resolve();
      try {
        await this.r.db(dbName).tableCreate(tableName, options);
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }

  tableDrop({ dbName, tableName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject("Error: Invalid Input | dbName | tableDrop");
      if (typeof tableName != "string") return reject("Error: Invalid Input | tableName | tableDrop");
      let dbList = this.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      if (!dbList.includes(dbName)) return reject(new Error(`Database ${dbName} does not exist.`));
      let tableList = this.tableList(dbName);
      try {
        tableList = await tableList;
      } catch (err) {
        return reject(err);
      }
      if (!tableList.includes(tableName)) return resolve();
      try {
        await this.r.db(dbName).tableDrop(tableName);
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }

  tableList({ dbName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject("Error: Invalid Input | dbName | tableList");
      let tableList = this.r.db(dbName).tableList();
      try {
        tableList = await tableList;
      } catch (err) {
        return reject(err);
      }
      return resolve(tableList);
    });
  }

  indexCreate({ dbName, tableName, indexName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject("Error: Invalid Input | dbName | indexCreate");
      if (typeof tableName != "string") return reject("Error: Invalid Input | tableName | indexCreate");
      if (typeof indexName != "string") return reject("Error: Invalid Input | indexName | indexCreate");
      let dbList = this.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      if (!dbList.includes(dbName)) return reject(new Error(`Database ${dbName} does not exist.`));
      let tableList = this.tableList({ dbName });
      try {
        tableList = await tableList;
      } catch (err) {
        return reject(err);
      }
      if (!tableList.includes(tableName)) return reject(new Error(`Table ${tableName} does not exist.`));
      let indexList = this.indexList({ dbName, tableName });
      try {
        indexList = await indexList;
      } catch (err) {
        return reject(err);
      }
      if (indexList.includes(indexName)) return resolve();
      try {
        await this.r
          .db(dbName)
          .table(tableName)
          .indexCreate(indexName);
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }

  indexDrop({ dbName, tableName, indexName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject("Error: Invalid Input | dbName | indexDrop");
      if (typeof tableName != "string") return reject("Error: Invalid Input | tableName | indexDrop");
      if (typeof indexName != "string") return reject("Error: Invalid Input | indexName | indexDrop");
      let dbList = this.dbList();
      try {
        dbList = await dbList;
      } catch (err) {
        return reject(err);
      }
      if (!dbList.includes(dbName)) return reject(new Error(`Database ${dbName} does not exist.`));
      let tableList = this.tableList({ dbName });
      try {
        tableList = await tableList;
      } catch (err) {
        return reject(err);
      }
      if (!tableList.includes(tableName)) return reject(new Error(`Table ${tableName} does not exist.`));
      let indexList = this.indexList({ dbName, tableName });
      try {
        indexList = await indexList;
      } catch (err) {
        return reject(err);
      }
      if (!indexList.includes(indexName)) return resolve();
      try {
        await this.r
          .db(dbName)
          .table(tableName)
          .indexDrop(indexName);
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }

  indexList({ dbName, tableName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject("Error: Invalid Input | dbName | indexList");
      if (typeof tableName != "string") return reject("Error: Invalid Input | tableName | indexList");
      let indexList = this.r
        .db(dbName)
        .table(tableName)
        .indexList();
      try {
        indexList = await indexList;
      } catch (err) {
        return reject(err);
      }
      return resolve(indexList);
    });
  }

  db({ dbName }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | db"));
      let db = this.r.db(dbName);
      try {
        db = await db;
      } catch (err) {
        return reject(err);
      }
      return resolve(db);
    });
  }

  table({ dbName, tableName, options = {} }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | table"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | table"));
      if (typeof options != "object") return reject("Error: Invalid Input | options | table");
      let table = this.r.db(dbName).table(tableName, options);
      try {
        table = await table;
      } catch (err) {
        return reject(err);
      }
      return resolve(table);
    });
  }

  get({ dbName, tableName, key }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | get"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | get"));
      let row = this.r
        .db(dbName)
        .table(tableName)
        .get(key);
      try {
        row = await row;
      } catch (err) {
        return reject(err);
      }
      return resolve(row);
    });
  }

  getAll({ dbName, tableName, keys, options = {} }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | getAll"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | getAll"));
      if (typeof options != "object") return reject("Error: Invalid Input | options");
      let rows = this.r
        .db(dbName)
        .table(tableName)
        .getAll(keys, options);
      try {
        rows = await rows;
      } catch (err) {
        return reject(err);
      }
      return resolve(rows);
    });
  }

  insert({ dbName, tableName, object, options = {} }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | insert"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | insert"));
      if (typeof object != "object") return reject("Error: Invalid Input | object | insert");
      if (typeof options != "object") return reject("Error: Invalid Input | options | insert");
      try {
        await this.r
          .db(dbName)
          .table(tableName)
          .insert(object, options);
      } catch (err) {
        return resolve(err);
      }
      return resolve();
    });
  }

  update({ dbName, tableName, object, options = {} }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | update"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | update"));
      if (typeof object != "object") return reject("Error: Invalid Input | object | update");
      if (typeof options != "object") return reject("Error: Invalid Input | options | update");
      try {
        await this.r
          .db(dbName)
          .table(tableName)
          .update(object, options);
      } catch (err) {
        return resolve(err);
      }
      return resolve();
    });
  }

  replace({ dbName, tableName, object, options = {} }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | replace"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | replace"));
      if (typeof object != "object") return reject("Error: Invalid Input | object | replace");
      if (typeof options != "object") return reject("Error: Invalid Input | options | replace");
      try {
        await this.r
          .db(dbName)
          .table(tableName)
          .replace(object, options);
      } catch (err) {
        return resolve(err);
      }
      return resolve();
    });
  }

  delete({ dbName, tableName, key }) {
    return new Promise(async (resolve, reject) => {
      if (typeof dbName != "string") return reject(new Error("Invalid Input | dbName | delete"));
      if (typeof tableName != "string") return reject(new Error("Invalid Input | tableName | delete"));
      try {
        await this.r
          .db(dbName)
          .table(tableName)
          .get(key)
          .delete();
      } catch (err) {
        return reject(err);
      }
      return resolve();
    });
  }
};
