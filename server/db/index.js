const RethinkDriver = require("./RethinkDriver");

class RethinkProvider extends RethinkDriver {
  init() {
    return new Promise(async (resolve, reject) => {
      await this.dbCreate({ dbName: "site" }).catch(reject);
      await this.tableCreate({ dbName: "site", tableName: "users", options: { primaryKey: "id" } }).catch(reject);
      await this.indexCreate({ dbName: "site", tableName: "users", indexName: "token" }).catch(reject);
      await this.tableCreate({ dbName: "site", tableName: "commands", options: { primaryKey: "id" } }).catch(reject);
      await this.loadConfig().catch(reject);
      return resolve();
    });
  }

  loadConfig() {
    return new Promise(async (resolve, reject) => {
      let configs = this.table({ dbName: "config", tableName: "config" });
      try {
        configs = await configs;
      } catch (err) {
        return reject(err);
      }
      this.config = {};
      for (let config of configs) {
        this.config[config.key] = config.value;
      }
      return resolve();
    });
  }

  checkAuth(token) {
    return new Promise(async (resolve, reject) => {
      if (token.slice(0, 7) != "Bearer ") return reject(new Error("Invalid Token Format"));
      token = token.slice(7);
      if (token == this.config.access_token_dbl) return resolve({ type: "dbl" });
      else if (token == this.config.access_token_bot) return resolve({ type: "bot" });
      let users = this.getAll({ dbName: "site", tableName: "users", keys: token, options: { index: "token" } });
      try {
        users = await users;
      } catch (err) {
        return reject(err);
      }
      if (users.length == 0) return reject("No user found");
      if (users.length > 1) reject("Multiple users found");
      let user = users[0];
      if (user.admin) return resolve({ type: "admin", user });
      return resolve({ type: "user", user });
    });
  }
}

const db = new RethinkProvider();
module.exports = db;
