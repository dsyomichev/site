const path = require("path");
const fs = require("fs");

const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const chalk = require("chalk");
const history = require("connect-history-api-fallback");
const favicon = require("serve-favicon");

const db = require(path.resolve("db"));

const dev = process.argv.includes("--dev");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

if (dev) app.use(morgan("dev"));

app.use(favicon(path.join(__dirname, "favicon.ico")));

const port = process.env.port || 8080;
const host = process.env.host || "localhost";

if (dev) console.log(chalk.red("\nSteamRadar API:"));

let routes = fs
  .readdirSync(path.resolve("routes"))
  .filter(() => {
    return fs.lstatSync(path.resolve("routes")).isDirectory();
  })
  .filter(x => {
    if (x.charAt(0) != "v" || !x.slice(1).match(/^[0-9]+$/g)) return false;
    return true;
  });

for (let version of routes) {
  let router = require(path.resolve(`routes/${version}`));
  router.get("/", (req, res, next) => {
    res.json({ version: parseInt(version.slice(1)) });
  });
  app.use(`/api/${version}/`, router);
  if (dev) console.log(chalk.blue(` - API ${version} - ${chalk.white(`http://${host}:${port}/api/${version}`)}`));
}

app.use(history());
app.use(express.static(path.join(__dirname)));

db
  .init()
  .then(() => {
    app.listen(port, host);
    console.log(chalk.green("--------------------------------------"));
  })
  .catch(err => {
    console.log("\nError:");
    console.log(chalk.red(err));
  });
