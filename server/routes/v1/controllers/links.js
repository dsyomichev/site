const path = require("path");

const db = require(path.resolve("db"));

module.exports = router => {
  router.get("/links", async (req, res) => {
    return res.status(200).json({
      links: {
        url_redirect: db.config.url_redirect,
        url_invite: db.config.url_invite,
        url_guild: db.config.url_guild,
        url_authorize: `https://discordapp.com/api/oauth2/authorize?client_id=${db.config.bot_id}&redirect_uri=${encodeURIComponent(
          db.config.url_redirect
        )}&response_type=code&scope=guilds%20identify`
      }
    });
  });
};
