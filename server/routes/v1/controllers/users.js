const path = require("path");
const axios = require("axios");
const btoa = require("btoa");
const UIDGenerator = require("uid-generator");

const db = require(path.resolve("db"));
const uidgen = new UIDGenerator(512, UIDGenerator.BASE62);

module.exports = router => {
  router.post("/users", async (req, res) => {
    if (!req.body.type) return res.status(400).json({ error: "Missing Param | type" });
    if (req.body.type == "upvote") {
      if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
      let auth = await db.checkAuth(req.headers.authorization).catch(err => res.status(400).json({ error: "Invalid Token" }));
      if (auth.type != "dbl" && auth.type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
      if (String(req.body.bot) != db.config.bot_id) return res.status(400).json({ error: "Invalid Param | bot" });
      try {
        await db.insert({ dbName: "site", tableName: "users", object: { id: String(req.body.user), upvote: true }, options: { conflict: "update" } });
      } catch (err) {
        return res.status(500).json({ error: "Internal Error" });
      }
      return res.status(200).json({ success: true });
    } else {
      return res.status(400).json({ error: "Invalid Param | type" });
    }
  });
  router.get("/users/login", async (req, res) => {
    if (!req.query.code) return res.status(400).json({ error: "Missing Parameter | code" });
    const creds = btoa(`${db.config.bot_id}:${db.config.bot_secret}`);
    const redirect = encodeURIComponent(db.config.url_redirect);
    let tokens = axios({
      method: "post",
      responseType: "json",
      url: `https://discordapp.com/api/oauth2/token?grant_type=authorization_code&code=${req.query.code}&redirect_uri=${redirect}`,
      headers: { Authorization: `Basic ${creds}` }
    });
    try {
      tokens = await tokens;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    let user = axios({
      method: "get",
      responseType: "json",
      url: "http://discordapp.com/api/users/@me",
      headers: { Authorization: `Bearer ${tokens.data.access_token}` }
    });
    try {
      user = await user;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    let saved = db.get({ dbName: "site", tableName: "users", key: user.data.id });
    try {
      saved = (await saved) || {};
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    let token = uidgen.generateSync();
    try {
      await db.insert({
        dbName: "site",
        tableName: "users",
        object: {
          id: user.data.id,
          access: tokens.data.access_token,
          refresh: tokens.data.refresh_token,
          token,
          admin: saved.admin || false,
          upvote: saved.upvote || false,
          patreon: saved.patreon || false
        },
        options: { conflict: "update" }
      });
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    res.status(200).json({
      user: {
        username: user.data.username,
        discriminator: user.data.discriminator,
        avatar: user.data.avatar,
        token,
        admin: saved.admin || false,
        upvote: saved.upvote || false,
        patreon: saved.patreon || false
      }
    });
  });
  router.get("/users/@me", async (req, res) => {
    if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
    let auth = await db.checkAuth(req.headers.authorization).catch(err => res.status(400).json({ error: "Invalid Token" }));
    if (auth.type != "user" && auth.type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
    let saved = auth.user;
    const creds = btoa(`${db.config.bot_id}:${db.config.bot_secret}`);
    const redirect = encodeURIComponent(db.config.url_redirect);
    let user = axios({
      method: "get",
      responseType: "json",
      url: "http://discordapp.com/api/users/@me",
      headers: { Authorization: `Bearer ${saved.access}` }
    });
    try {
      user = await user;
    } catch (err) {
      return res.status(500).json({ error: "Expired" });
    }
    return res.status(200).json({
      user: {
        username: user.data.username,
        discriminator: user.data.discriminator,
        avatar: user.data.avatar,
        token: saved.token,
        admin: saved.admin || false,
        upvote: saved.upvote || false,
        patreon: saved.patreon || false
      }
    });
  });
  router.get("/users/:id/upvote", async (req, res) => {
    if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
    let auth = await db.checkAuth(req.headers.authorization).catch(err => res.status(400).json({ error: "Invalid Token" }));
    if (auth.type != "bot" && auth.type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
    let user = db.get({ dbName: "site", tableName: "users", key: req.params.id });
    try {
      user = await user;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    if (!user) return res.status(400).json({ error: "User not found." });
    return res.status(200).json({ upvote: user.upvote || false });
  });
  router.get("/users/:id/patreon", async (req, res) => {
    if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
    let auth = await db.checkAuth(req.headers.authorization).catch(err => res.status(400).json({ error: "Invalid Token" }));
    if (auth.type != "bot" && auth.type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
    let user = db.get({ dbName: "site", tableName: "users", key: req.params.id });
    try {
      user = await user;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    if (!user) return res.status(400).json({ error: "User not found." });
    return res.status(200).json({ patreon: user.patreon || false });
  });
};
