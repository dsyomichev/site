const path = require("path");

const db = require(path.resolve("db"));

module.exports = router => {
  router.get("/commands", async (req, res, next) => {
    let commands = db.table({ dbName: "site", tableName: "commands" });
    try {
      commands = await commands;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    return res.status(200).json({ commands });
  });

  router.post("/commands", async (req, res, next) => {
    if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
    if ((await db.checkAuth(req.headers.authorization)).type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
    if (!req.body.command || typeof req.body.command != "object") return res.status(400).json({ error: "Invalid Command" });
    let { name, format, examples = [], details, category, api = false, upvote = false, patreon = false, owner = false, description } = req.body.command;
    api = Boolean(api);
    upvote = Boolean(upvote);
    patreon = Boolean(patreon);
    owner = Boolean(owner);
    if (!name || typeof name != "string") return res.status(400).json({ error: "Invalid Param | name" });
    if (!format || typeof format != "string") return res.status(400).json({ error: "Invalid Param | format" });
    if (!details || typeof details != "string") return res.status(400).json({ error: "Invalid Param | details" });
    if (!description || typeof description != "string") return res.status(400).json({ error: "Invalid Param | description" });
    if (!category || typeof category != "string") return res.status(400).json({ error: "Invalid Param | category" });
    if (!Array.isArray(examples)) return res.status(400).json({ error: "Invalid Param | examples" });
    let commands = db.table({ dbName: "site", tableName: "commands" });
    try {
      commands = await commands;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    commands = commands.map(command => {
      return command.id;
    });
    let id = Math.floor(Math.random() * (999 - 100 + 1) + 100);
    while (commands.includes(id)) id = Math.floor(Math.random() * (999 - 100 + 1) + 100);
    req.body.command.id = id;
    try {
      await db.insert({ dbName: "site", tableName: "commands", object: req.body.command });
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    return res.status(200).json({ command: req.body.command });
  });

  router.get("/commands/:id", async (req, res, next) => {
    let command = db.get({ dbName: "site", tableName: "commands", key: parseInt(req.params.id) });
    try {
      command = await command;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    if (!command) return res.status(404).json({ error: "Command not found." });
    return res.status(200).json({ command });
  });

  router.put("/commands/:id", async (req, res, next) => {
    if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
    if ((await db.checkAuth(req.headers.authorization)).type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
    if (!req.body.command || typeof req.body.command != "object") return res.status(400).json({ error: "Invalid Command" });
    let { id, name, format, examples, details, category, api, upvote, patreon, owner, description } = req.body.command;
    if (!id) return res.status(400).json({ error: "Invalid Param | id" });
    if (id != parseInt(req.params.id)) return res.status(400).json({ error: "Command ID does not match URL Param ID." });
    if (name && typeof name != "string") return res.status(400).json({ error: "Invalid Param | name" });
    if (format && typeof format != "string") return res.status(400).json({ error: "Invalid Param | format" });
    if (details && typeof details != "string") return res.status(400).json({ error: "Invalid Param | details" });
    if (description && typeof description != "string") return res.status(400).json({ error: "Invalid Param | description" });
    if (category && typeof category != "string") return res.status(400).json({ error: "Invalid Param | category" });
    if (examples && !Array.isArray(examples)) return res.status(400).json({ error: "Invalid Param | examples" });
    if (api || api === false) api = Boolean(api);
    if (upvote || upvote === false) upvote = Boolean(upvote);
    if (patreon || patreon === false) patreon = Boolean(patreon);
    if (owner || owner === false) owner = Boolean(owner);
    let command = db.get({ dbName: "site", tableName: "commands", key: id });
    try {
      command = await command;
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    if (!command) return res.status(400).json({ error: "Command does not exist." });
    try {
      await db.update({ dbName: "site", tableName: "commands", object: req.body.command });
    } catch (err) {
      return res.status(500).json({ error: "Internal Error" });
    }
    return res.status(200).json({ command: req.body.command });
  });

  router.delete("/commands/:id", async (req, res, next) => {
    if (!req.headers.authorization) return res.status(401).json({ error: "Unauthorized" });
    if ((await db.checkAuth(req.headers.authorization)).type != "admin") return res.status(403).json({ error: "Invalid Authorization" });
    try {
      await db.delete({ dbName: "site", tableName: "commands", key: parseInt(req.params.id) });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ error: "Internal Error" });
    }
    return res.status(200).json({ success: true });
  });
};
