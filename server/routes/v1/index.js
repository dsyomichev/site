const path = require("path");
const router = require("express").Router();

require("fs")
  .readdirSync(path.resolve(`routes/v1/controllers/`))
  .forEach(file => {
    if (file.slice(-3) != ".js") return;
    require(path.resolve(`routes/v1/controllers/${file}`))(router);
  });

module.exports = router;
