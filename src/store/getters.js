export default {
  user: state => {
    return state.user;
  },
  commands: state => ({ type = "all" } = {}) => {
    if (type == "all") return state.commands;
    else if (type == "store") return state.commands.filter(command => command.category == "Steam Store");
    else if (type == "community") return state.commands.filter(command => command.category == "Steam Community");
    else if (type == "info") return state.commands.filter(command => command.category == "Config & Info");
  },
  command: state => ({ id }) => {
    return state.commands.find(command => command.id == id);
  },
  links: state => {
    return state.links;
  }
};
