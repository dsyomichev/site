export default {
  setUser: (state, { user }) => {
    user.auth = true;
    state.user = user;
  },
  removeUser: state => {
    state.user = { auth: false };
  },
  setCommands: (state, { commands }) => {
    state.commands = commands || state.commands;
  },
  addCommand: (state, { command }) => {
    state.commands.push(command);
  },
  editCommand: (state, { command }) => {
    let index = state.commands
      .map(function(e) {
        return e.id;
      })
      .indexOf(command.id);

    state.commands.splice(index, 1, command);
  },
  removeCommand: (state, { id }) => {
    let index = state.commands
      .map(function(e) {
        return e.id;
      })
      .indexOf(id);

    state.commands.splice(index, 1);
  },
  setLinks: (state, { links }) => {
    state.links = links;
  }
};
