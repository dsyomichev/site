import service from "../service";

export default {
  getAllCommands: ({ commit }) => {
    return new Promise(async (resolve, reject) => {
      let commands;
      try {
        commands = await service.commands.all();
      } catch (err) {
        return resolve(err);
      }
      commit("setCommands", { commands });
      return resolve();
    });
  },
  newCommand({ commit, getters }, { command }) {
    return new Promise(async (resolve, reject) => {
      let user = getters.user;
      if (!user.auth || !user.admin) reject(new Error("User is not authenticated or does not have propper authentication."));
      let new_command;
      try {
        new_command = await service.commands.create({ command, token: user.token });
      } catch (err) {
        return reject(err);
      }
      commit("addCommand", { command: new_command });
      return resolve();
    });
  },
  editCommand({ commit, getters }, { command }) {
    return new Promise(async (resolve, reject) => {
      let user = getters.user;
      if (!user.auth || !user.admin) reject(new Error("User is not authenticated or does not have propper authentication."));
      let new_command;
      try {
        new_command = await service.commands.edit({ command, token: user.token });
      } catch (err) {
        return reject(err);
      }
      commit("editCommand", { command: new_command });
      return resolve();
    });
  },
  removeCommand({ commit, getters }, { id }) {
    return new Promise(async (resolve, reject) => {
      let user = getters.user;
      if (!user.auth || !user.admin) reject(new Error("User is not authenticated or does not have propper authentication."));
      try {
        await service.commands.remove({ id, token: user.token });
      } catch (err) {
        return reject(err);
      }
      commit("removeCommand", { id });
      return resolve();
    });
  },
  getLinks({ commit }) {
    return new Promise(async (resolve, reject) => {
      let links;
      try {
        links = await service.links.all();
      } catch (err) {
        return reject(err);
      }
      commit("setLinks", { links });
      return resolve();
    });
  },
  login({ commit }, { code }) {
    return new Promise(async (resolve, reject) => {
      let user = service.users.login({ code });
      try {
        user = await user;
      } catch (err) {
        return reject(err);
      }
      localStorage.setItem("token", user.token);
      commit("setUser", { user });
      return resolve();
    });
  },
  loadUser({ commit }, { token }) {
    return new Promise(async (resolve, reject) => {
      let user = service.users.me({ token });
      try {
        user = await user;
      } catch (err) {
        localStorage.removeItem("token");
        return reject(err);
      }
      localStorage.setItem("token", user.token);
      commit("setUser", { user });
      return resolve();
    });
  },
  logout({ commit }) {
    localStorage.removeItem("token");
    return commit("removeUser");
  }
};
