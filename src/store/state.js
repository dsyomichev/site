export default {
  user: {
    auth: false
  },
  commands: [],
  links: {
    redirect_uri: undefined,
    invite_url: undefined,
    server_url: undefined
  }
};
