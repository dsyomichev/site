import api from "./api";
export default {
  login: ({ code }) => {
    return new Promise(async (resolve, reject) => {
      let user = api({ method: "get", responseType: "json", url: "/users/login", params: { code } });
      try {
        user = await user;
      } catch (err) {
        return reject(err);
      }
      return resolve(user.data.user);
    });
  },
  me: ({ token }) => {
    return new Promise(async (resolve, reject) => {
      let user = api({ method: "get", responseType: "json", url: "/users/@me", headers: { Authorization: `Bearer ${token}` } });
      try {
        user = await user;
      } catch (err) {
        return reject(err);
      }
      return resolve(user.data.user);
    });
  }
};
