import users from "./users";
import commands from "./commands";
import links from "./links";
export default { users, commands, links };
