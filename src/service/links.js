import api from "./api";
export default {
  all: () => {
    return new Promise(async (resolve, reject) => {
      let response;
      try {
        response = await api.get("/links");
      } catch (err) {
        return reject(err);
      }
      if (response.data.error) return reject(new Error(response.data.error));
      return resolve(response.data.links);
    });
  }
};
