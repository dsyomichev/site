import api from "./api";
export default {
  all: () => {
    return new Promise(async (resolve, reject) => {
      let response;
      try {
        response = await api.get("/commands");
      } catch (err) {
        return reject(err);
      }
      if (response.data.error) return reject(new Error(response.data.error));
      return resolve(response.data.commands);
    });
  },
  create: ({ command, token }) => {
    return new Promise(async (resolve, reject) => {
      let response;
      try {
        response = await api({ method: "post", url: "/commands", data: { command }, headers: { Authorization: `Bearer ${token}` } });
      } catch (err) {
        return reject(err);
      }
      if (response.data.error) return reject(new Error(response.data.error));
      return resolve(response.data.command);
    });
  },
  edit: ({ command, token }) => {
    return new Promise(async (resolve, reject) => {
      let response;
      try {
        response = await api({ method: "put", url: `/commands/${command.id}`, data: { command }, headers: { Authorization: `Bearer ${token}` } });
      } catch (err) {
        return reject(err);
      }
      if (response.data.error) return reject(new Error(response.data.error));
      return resolve(response.data.command);
    });
  },
  remove: ({ id, token }) => {
    return new Promise(async (resolve, reject) => {
      let response;
      try {
        response = await api({ method: "delete", url: `/commands/${id}`, headers: { Authorization: `Bearer ${token}` } });
      } catch (err) {
        return reject(err);
      }
      if (response.data.error) return reject(new Error(response.data.error));
      return resolve();
    });
  }
};
