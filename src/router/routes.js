import admin_routes from "./admin";
import public_routes from "./public";
import error_routes from "./error";

export default Array.concat(admin_routes, public_routes, error_routes);
