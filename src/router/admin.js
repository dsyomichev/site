import store from "../store";
import service from "../service";

import Panel from "@/components/Admin/Panel";
import Commands from "@/components/Admin/Commands/Commands";
import CreateCommand from "@/components/Admin/Commands/CreateCommand";
import EditCommand from "@/components/Admin/Commands/EditCommand";
import CommandList from "@/components/Admin/Commands/CommandList";

export default [
  {
    path: "/panel",
    component: Panel,
    children: [
      {
        name: "panel",
        path: ""
      },
      {
        path: "commands",
        component: Commands,
        children: [
          {
            name: "commandlist",
            path: "",
            component: CommandList
          },
          {
            name: "createcommand",
            path: "create",
            component: CreateCommand
          },
          {
            name: "editcommand",
            path: ":id/edit",
            component: EditCommand
          }
        ]
      }
    ],
    beforeEnter: (to, from, next) => {
      let user = store.getters.user;
      if (!user.auth) return next({ name: "401" });
      if (!user.admin) return next({ name: "403" });
      return next();
    }
  }
];
