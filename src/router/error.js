import err_404 from "@/components/Error/404";
import err_403 from "@/components/Error/403";
import err_401 from "@/components/Error/401";
export default [{ name: "401", path: "/401", component: err_401 }, { name: "403", path: "/403", component: err_403 }, { name: "404", path: "*", component: err_404 }];
