import store from "../store";
import service from "../service";

import Home from "@/components/Public/Home";
import Commands from "@/components/Public/Commands/Commands";
import StoreCommands from "@/components/Public/Commands/StoreCommands";
import CommunityCommands from "@/components/Public/Commands/CommunityCommands";
import InfoCommands from "@/components/Public/Commands/InfoCommands";
import Command from "@/components/Public/Commands/Command";
import Help from "@/components/Public/Help/Help";

export default [
  {
    name: "login",
    path: "/login",
    beforeEnter: async (to, from, next) => {
      if (!to.query.code) return (window.location.href = store.getters.links.url_authorize);
      await store.dispatch("login", { code: to.query.code });
      return next({ name: "home" });
    }
  },
  {
    name: "logout",
    path: "/logout",
    beforeEnter: (to, from, next) => {
      store.dispatch("logout");
      return next({ name: "home" });
    }
  },
  {
    name: "home",
    path: "/",
    component: Home
  },
  {
    path: "/commands",
    component: Commands,
    children: [
      {
        path: "",
        name: "commands",
        component: StoreCommands
      },
      {
        path: "store",
        name: "storecommands",
        component: StoreCommands
      },
      {
        path: "community",
        name: "communitycommands",
        component: CommunityCommands
      },
      {
        path: "info",
        name: "infocommands",
        component: InfoCommands
      }
    ]
  },
  {
    name: "command",
    path: "/commands/:id",
    component: Command
  },
  {
    name: "help",
    path: "/help",
    component: Help
  }
];
