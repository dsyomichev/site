// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import service from "./service";
import store from "./store";

Vue.config.productionTip = false;

require("@/assets/sass/main.scss");

(async () => {
  let load = [store.dispatch("getLinks"), store.dispatch("getAllCommands")];
  let token = localStorage.getItem("token");
  if (token) load.push(store.dispatch("loadUser", { token }));
  await Promise.all(load);
  new Vue({
    el: "#app",
    router,
    store,
    components: { App },
    template: "<App/>"
  });
})();
